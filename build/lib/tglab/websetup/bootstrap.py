# -*- coding: utf-8 -*-
"""Setup the tglab application"""
from __future__ import print_function, unicode_literals
import transaction
from tglab import model


def bootstrap(command, conf, vars):
    """Place any commands to setup tglab here"""

    # <websetup.bootstrap.before.auth
    from sqlalchemy.exc import IntegrityError
    try:
        u = model.User()
        u.user_name = 'fakir'
        u.display_name = 'FAKIR Marouane'
        u.email_address = 'm.fakir@cdgcapitalgestion.ma'
        u.password = 'mero2020'

        model.DBSession.add(u)

        g = model.Group()
        g.group_name = 'managers'
        g.display_name = 'Datastore managers Group'

        g.users.append(u)

        model.DBSession.add(g)

        p = model.Permission()
        p.permission_name = 'manage'
        p.description = 'This permission gives an administrative right'
        p.groups.append(g)

        p2 = model.Permission()
        p2.permission_name = 'consult'
        p2.description = 'Permission for consulting dédiée aux collaborateurs internes'
        p2.groups.append(g)

        model.DBSession.add(p)

        u1 = model.User()
        u1.user_name = 'editor'
        u1.display_name = 'Example editor'
        u1.email_address = 'editor@somedomain.com'
        u1.password = 'editpass'

        u2 = model.User()
        u2.user_name = 'sophia'
        u2.display_name = 'Hachki Sophia'
        u2.email_address = 'hacki-sophia@cdgcapitalgestion.ma'
        u2.password = 'sophiapass'

        u3 = model.User()
        u3.user_name = 'ckg'
        u3.display_name = 'Collaborateur CKG'
        u3.email_address = 'ckg@cdgcapitalgestion.ma'
        u3.password = 'ckgpass'

        model.DBSession.add(u1)
        model.DBSession.add(u2)
        model.DBSession.add(u3)
        model.DBSession.flush()
        transaction.commit()
    except IntegrityError:
        print('Warning, there was a problem adding your auth data, '
              'it may have already been added:')
        import traceback
        print(traceback.format_exc())
        transaction.abort()
        print('Continuing with bootstrapping...')

    # <websetup.bootstrap.after.auth>
