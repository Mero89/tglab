import tg

import tglab.model as db
from tglab.lib.base import BaseRestController


__all__ = ['ReferentielServiceController']


class RefMixin(BaseRestController):

    _model = None

    @tg.expose('json')
    def get_one(self, entity_id: str):
        rv = {self._model.__tablename__: self._model.by_id(entity_id)}
        return rv

    @tg.expose('json')
    @tg.decorators.paginate('data', items_per_page=100)
    def get_all(self):
        tname = self._model.__qualname__
        rv = {
            'model_name': tname,
            'data': self._model.get_all().all()
        }
        return rv


class Fonds(RefMixin):
    _model = db.Fonds


class Indice(RefMixin):
    _model = db.Indice


class Titre(RefMixin):
    _model = db.Titre


class Devise(RefMixin):
    _model = db.Devise


class Pays(RefMixin):
    _model = db.Pays


class Emetteur(RefMixin):
    _model = db.Emetteur


class Secteur(RefMixin):
    _model = db.Secteur


class Benchmark(RefMixin):
    _model = db.Benchmark


class FondsMarket(RefMixin):
    _model = db.FondsMarket


class ReferentielServiceController(BaseRestController):

    # @tg.expose('json')
    # def index(self):
    #     print(url('/api/referentiel/fonds', params={'page': 2}))
    #     redirect('/api/referentiel/fonds', params={'page': 2})

    fonds = Fonds()
    indice = Indice()
    titre = Titre()
    devise = Devise()
    pays = Pays()
    emetteur = Emetteur()
    secteur = Secteur()
    benchmark = Benchmark()
    fonds_market = FondsMarket()
