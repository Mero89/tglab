import tempfile
import zipfile
import datetime
import pathlib
import shutil

import tg

from tglab.lib.base import BaseRestController
from tglab.lib import PUBLIC_PATH
import tglab.model as db

# from hexagon.analytics import RatiosEngine
from hexagon.reporting import write_reporting
from hexagon.reporting.reporting_engine import TemplateEngineGenerator
from hexagon.veille import write_veille_analytics
from hexagon.utils import to_date, to_bool


DEFAULT_TEMPLATE_DIRECTORY = PUBLIC_PATH.joinpath('datastore/templates')
DEFAULT_OUTPUT_DIRECTORY = PUBLIC_PATH.joinpath('datastore/output')


__all__ = ['ReportingService']


class ReportingService(BaseRestController):

    @tg.expose(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    def etat_opcvm(self, code_fonds, date_debut, date_fin):
        meth = tg.request.method
        fname = f'{code_fonds.lower()}_data.xlsx'
        # Pour fixer le nom du fichier à envoyer
        content_disposition = f'attachment;filename={fname}'
        if meth == 'POST':
            rq = tg.request.POST
            tg.response.headers['Content-Disposition'] = content_disposition
            
            code_fonds = rq['code_fonds']
            debut = datetime.date.fromisoformat(rq['date_debut'])
            fin = datetime.date.fromisoformat(rq['date_fin'])
            
            obuff = write_reporting('', code_fonds, date_reporting=fin, date_reference=debut, in_memory=True)
            obuff.seek(0)
            return obuff

        elif meth == 'GET':
            tg.response.headers['Content-Disposition'] = content_disposition

            code_fonds = code_fonds
            debut = to_date(date_debut)
            fin = to_date(date_fin)

            obuff = write_reporting('', code_fonds, date_reporting=fin, date_reference=debut, in_memory=True)
            obuff.seek(0)
            return obuff
        else:
            return tg.abort(403, f"Methode {meth} non authorisée")

    @tg.expose(content_type='application/zip')
    def etat_opcvm_multi(self, liste_fonds: list, date_debut, date_fin):
        if tg.request.method == 'POST':
            rq = tg.request.POST
            lfonds = [x.strip().upper() for x in rq['liste_fonds']]
            debut = datetime.date.fromisoformat(rq['date_debut'])
            fin = datetime.date.fromisoformat(rq['date_fin'])

        elif tg.request.method == 'GET':
            lfonds = [x.strip().upper() for x in liste_fonds[1:-1].split(',')]
            print(lfonds)
            debut = to_date(date_debut)
            fin = to_date(date_fin)
            
        else:
            return tg.abort(403, f"Methode {tg.request.method} non authorisée")

        public_tempdir_root = PUBLIC_PATH.joinpath('datastore')
        tempdir = tempfile.TemporaryDirectory(dir=public_tempdir_root)
        path_tempdir = pathlib.Path(tempdir.name)
        
        for cfonds in lfonds:
            fname = f'{cfonds.lower()}_data.xlsx'
            fpath = path_tempdir.joinpath(fname)
            write_reporting(fpath, cfonds, date_reporting=fin, date_reference=debut, in_memory=False)

        zipname = f"rapport global du {debut} au {fin}.zip"
        path_zipfile = path_tempdir.joinpath(zipname)
        tempfiles = tuple(path_tempdir.iterdir())
        with zipfile.ZipFile(path_zipfile, mode='w') as z:
            for el in tempfiles:
                if el.name.endswith('.xlsx'):
                    z.write(el, el.name)
            
            print(f'Zip File {zipname} created')

        here_zip_file = PUBLIC_PATH.joinpath('datastore').joinpath(zipname)
        shutil.move(path_zipfile, here_zip_file)
        tempdir.cleanup()

        content_disposition = f'attachment;filename={zipname}'
        tg.response.headers['Content-Disposition'] = content_disposition
        fzip = open(here_zip_file, 'rb')
        return fzip

    @tg.expose(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    def veille_marche(self, date_debut, date_fin, audit=True):
        meth = tg.request.method
        fname = f'veille_report.xlsx'
        # Pour fixer le nom du fichier à envoyer
        content_disposition = f'attachment;filename={fname}'
        if meth == 'POST':
            rq = tg.request.POST
            resp = tg.response
            resp.headers['Content-Disposition'] = content_disposition
            
            debut = datetime.date.fromisoformat(rq['date_debut'])
            fin = datetime.date.fromisoformat(rq['date_fin'])
            
            obuff = write_veille_analytics('', debut=debut, fin=fin, audit=to_bool(audit), in_memory=True)
            obuff.seek(0)
            return obuff

        elif meth == 'GET':
            debut = to_date(date_debut)
            fin = to_date(date_fin)

            obuff = write_veille_analytics('', debut=debut, fin=fin, audit=to_bool(audit), in_memory=True)
            obuff.seek(0)
            return obuff
        else:
            return tg.abort(403, f"Methode {meth} non authorisée")

    @tg.expose(content_type='application/zip')
    def auto_generate_one(self, code_fonds, date_debut, date_fin, periode='MENSUEL'):
        if tg.request.method == 'POST':
            rq = tg.request.POST
            fonds = db.Fonds.by_code(rq['code_fonds'])
            debut = datetime.date.fromisoformat(rq['date_debut'])
            fin = datetime.date.fromisoformat(rq['date_fin'])
            periode = rq['periode']

        elif tg.request.method == 'GET':
            fonds = db.Fonds.by_code(code_fonds)
            debut = to_date(date_debut)
            fin = to_date(date_fin)
            periode = periode
            
        else:
            return tg.abort(403, f"Methode {tg.request.method} non authorisée")
        
        # Temporary output Folder
        temporary_output_directory = tempfile.TemporaryDirectory(dir=DEFAULT_OUTPUT_DIRECTORY)
        path_output_directory = pathlib.Path(temporary_output_directory.name)
        teg = TemplateEngineGenerator(
            periode=periode,
            output_dir=path_output_directory,
            templates_dir=DEFAULT_TEMPLATE_DIRECTORY
        )
        teg.auto_fill_one(fonds, fin, debut, periode)

        zipname = f"Reporting-{fonds.code} du {debut} au {fin}.zip"
        path_zipfile = path_output_directory.joinpath(zipname)
        tempfiles = tuple(path_output_directory.iterdir())
        
        with zipfile.ZipFile(path_zipfile, mode='w') as z:
            for el in tempfiles:
                if el.name.endswith('.xlsx'):
                    z.write(el, el.name)
            
            print(f'Zip File {zipname} created')

        here_zip_file = PUBLIC_PATH.joinpath('datastore').joinpath(zipname)
        shutil.move(path_zipfile, here_zip_file)
        temporary_output_directory.cleanup()

        content_disposition = f'attachment;filename={zipname}'
        tg.response.headers['Content-Disposition'] = content_disposition
        fzip = open(here_zip_file, 'rb')
        return fzip

    @tg.expose(content_type='application/zip')
    def auto_generate_multi(self, date_debut, date_fin, periode='MENSUEL', liste_fonds=None, categorie=None):
        if tg.request.method == 'POST':
            rq = tg.request.POST
            lfonds = [x.strip().upper() for x in rq['liste_fonds']]
            debut = datetime.date.fromisoformat(rq['date_debut'])
            fin = datetime.date.fromisoformat(rq['date_fin'])
            periode = rq.get('periode')
            categorie = rq.get('categorie')

        elif tg.request.method == 'GET':
            if liste_fonds:
                cfonds = [x.strip().upper() for x in liste_fonds[1:-1].split(',')]
                lfonds = [db.Fonds.by_code(x) for x in cfonds]
            else:
                lfonds = None
            debut = to_date(date_debut)
            fin = to_date(date_fin)
            periode = periode
            categorie = categorie
            
        else:
            return tg.abort(403, f"Methode {tg.request.method} non authorisée")
        # Temporary output Folder
        temporary_output_directory = tempfile.TemporaryDirectory(dir=DEFAULT_OUTPUT_DIRECTORY)
        path_output_directory = pathlib.Path(temporary_output_directory.name)
        teg = TemplateEngineGenerator(
            periode=periode,
            output_dir=path_output_directory,
            templates_dir=DEFAULT_TEMPLATE_DIRECTORY
        )
        teg.auto_fill_many(fin, debut, periode, lfonds=lfonds, categorie=categorie)

        zipname = f"Reporting-filled du {debut} au {fin}.zip"
        path_zipfile = path_output_directory.joinpath(zipname)
        tempfiles = tuple(path_output_directory.iterdir())
        
        with zipfile.ZipFile(path_zipfile, mode='w') as z:
            for el in tempfiles:
                if el.name.endswith('.xlsx'):
                    z.write(el, el.name)
            
            print(f'Zip File {zipname} created')

        here_zip_file = PUBLIC_PATH.joinpath('datastore').joinpath(zipname)
        shutil.move(path_zipfile, here_zip_file)
        temporary_output_directory.cleanup()

        content_disposition = f'attachment;filename={zipname}'
        tg.response.headers['Content-Disposition'] = content_disposition
        fzip = open(here_zip_file, 'rb')
        return fzip
