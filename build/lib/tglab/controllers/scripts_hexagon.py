"""Documentation of Hexagon Core"""
import pathlib
import datetime
import operator as op

import tg
import tglab.model as db
from tglab.lib.base import BaseController

from hexagon.scripts.axa_rapports import generate_inventaire_axa


__all__ = ['ScriptsHexagon']


class ScriptsHexagonController(BaseController):
    # Uncomment this line if your controller requires an authenticated user
    # allow_only = predicates.not_anonymous()
    page_title = "Repository - Operations"

    @tg.expose('tglab.templates.forms.rapports_axa')
    def rapports_axa(self, **kw):
        # Génère les rapports spécifiques à Axa
        page_title = 'Rapports AXA Assurances'
        form_ctxt = self.get_form_ctxt()
        form_help = self.get_help_ctxt()
        form_dico = {
            'dt_inventaire': form_ctxt['maxdate'],
            'form_title': 'Extraction Inventaire AXA'
        }
        req_dico = tg.request.GET
        req_dico['method'] = tg.request.method
        rv = {
            'page': 'rapports_axa',
            'page_title': page_title,
            'form_dico': form_dico,
            'form_ctxt': form_ctxt,
            'form_help': form_help,
            'req_dico': req_dico
        }
        return rv

    @tg.expose(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    def generate_operations(self, **kw):
        meth = tg.request.method
        if meth != 'POST':
            return tg.abort(404, f"Methode {meth} non authorisée")
        
        rq = tg.request.POST
        fin = datetime.date.fromisoformat(rq['dt_inventaire'])
        resp = tg.response
        fname = 'inventaire_axa.xlsx'
        resp.headers['Content-Disposition'] = f'attachment;filename={fname}'
        obuff = generate_inventaire_axa('', date_marche=fin, promoteur='AXA', in_memory=True)
        obuff.seek(0)
        return obuff

    def get_form_ctxt(self):
        fonds_axa = db.Fonds.by_promoteur('AXA')
        
        def get_max_date(fonds):
            q = db.DBSession.query(
                db.func.max(db.EtatFonds.date_marche)
            ).filter(
                db.Fonds.id == db.EtatFonds.id_fonds,
                db.Fonds.id == fonds.id
            )
            r = q.first()
            return r[0]

        maxdate = get_max_date(fonds_axa[0])

        rv = {
            'ctxt_title': 'Infos contextuelles',
            'fonds': [x.description for x in fonds_axa],
            'maxdate': maxdate
        }

        return rv

    def get_help_ctxt(self):
        rv = {
            'help_title': 'Aide sur le rapport',
            'help_text': "<div>Choisir la date d'inventaire pour générer le fichier Excel au format AXA </div>",
        }
        return rv
