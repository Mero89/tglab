import zope.sqlalchemy
from sqlalchemy.orm import aliased
from sqlalchemy import desc, asc, func, or_
from sqlalchemy.exc import DatabaseError, OperationalError, IntegrityError, InvalidRequestError, SQLAlchemyError
from sqlalchemy.orm.exc import DetachedInstanceError, FlushError, MultipleResultsFound, NoResultFound

from hexagon import db

DBSession = db.DBSession
zope.sqlalchemy.register(DBSession)

DeclarativeBase = db.BaseEntity
metadata = DeclarativeBase.metadata
metadata.schema = 'public'

from hexagon.db.db_utils import (
    apply_date_filter,
    as_dict,
    filter_table,
    get_fields,
    get_fields_with_type,
    get_tables_headers,
    stream_query,
    subclasses,
    tablename_model
)

#####
# Generally you will not want to define your table's mappers, and data objects
# here in __init__ but will want to create modules them in the model directory
# and import them at the bottom of this file.
######


def init_model(engine):
    """Call me before using any of the tables or classes in the model."""
    DBSession.configure(bind=engine)
    return DBSession

# Import your model modules here.
from tglab.model.auth import User, Group, Permission
from hexagon.db.models import *

__all__ = (
    'Benchmark',
    'BenchmarkComposition',
    'BenchmarkFonds',
    'CompositionMasi',
    'CompositionMBI',
    'Courbe',
    'Devise',
    'DeviseCours',
    'Echeancier',
    'Emetteur',
    'EtatFonds',
    'EtatFondsMarket',
    'EtatTitre',
    'Fonds',
    'FondsMarket',
    'Group',
    'Indice',
    'IndiceValue',
    'InventaireFonds',
    'MetaApp',
    'MetaFonds',
    'Operation',
    'OperationPassif',
    'Pays',
    'Permission',
    'PosteOperation',
    'RawMBI',
    'RawTMP',
    'SCDFondsMarket',
    'Secteur',
    'SplitFondsMarket',
    'Titre',
    'TitreMarket',
    'User',
)
