function update_date_ytd(date_select) {
    var date = new Date(date_select);
    var year = date.getFullYear();
    return {'year': year, 'date': date}
    /*api_url = new URL('ds/ytd?year='+ year);
    $getJSON()*/
}
