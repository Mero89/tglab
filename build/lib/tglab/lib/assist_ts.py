"""Module d'inspection des séries chronologiques"""
import tglab.model as db


def assist_fonds_max_date(code_fonds: str=None):
    select = db.DBSession.query(db.func.max(db.EtatFonds.date_marche))
    cfonds = [x.upper() for x in code_fonds]
    if code_fonds:
        select = select.filter(db.EtatFonds.id == db.Fonds.id, db.Fonds.code.in_(cfonds))
    rv = select.first()
    return rv
