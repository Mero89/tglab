import tw2
import tw2.core as twc
import tw2.forms as twf

import tg

import tglab.model as db


class DeviseDropList(twf.FormField):
    pass


class FondsDropList:
    pass


class SDGMarketDropList:
    pass


class PeriodesDropList:
    pass


class IntervalDropList:
    pass


class PaysDropList:
    pass


class TitreDropList:
    pass


class DatesRangeWidget:
    # Permet de choisir parmi une liste de dates valides pour une TSeries.
    pass


class TableStatWidget:
    # Tableau Pour afficher des stats multiples d'une manière uniforme (Stat Block Concept)
    pass
