# -*- coding: utf-8 -*-
import pathlib

PROJECT_PATH = pathlib.Path(__file__).absolute().parent.parent

PUBLIC_PATH = PROJECT_PATH.joinpath('public/')
