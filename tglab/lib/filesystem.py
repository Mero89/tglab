"""
Implémente une classe de gestion des opérations récurrentes pour gérer les fichiers
sur système de fichiers.
"""
import os


class FileProvider(object):

    _supported_extensions = (
        'txt', 'xls', 'xlsx', 'csv', 'json', 'yaml',
        'yml', 'pkl', 'pdf', 'html', 'css')

    def __init__(self, path, ext):
        # if os.path.isdir(path):
        self._path = None
        self._ext = None
        self.ext = ext
        self.path = path

    @property
    def ext(self):
        return self._ext

    @ext.setter
    def ext(self, value):
        if self.is_supported_extension(value):
            self._ext = value.lower()
            return
        msg = "Extension doit être l'une des: {support}".format(support=self._supported_extensions)
        raise ValueError(msg)

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        if not os.path.isdir(value):
            msg = "Chemin du DOSSIER invalide!!!"
            raise ValueError(msg)
        self._path = value

    def iter_files(self):
        with os.scandir(self.path) as sc:
            for entry in sc:
                if self.is_valid_file_entry(entry):
                    yield entry

    def build_path_names(self, *lnames):
        """Construit une liste de chemins à partir des noms de fichiers fournis
        >> build_path_names('toto', 'pop') --> yields from (/path/to/toto.ext, /path/to/pop.ext)
        Args:
            *lnames: a list of names comma separated.
        
        Yields:
            str: Path of file with extension defined at Object instanciation.
        """
        for name in lnames:
            fname, *_ = name.split('.')
            fname += '.' + self.ext
            fpath = os.path.join(self.path, fname)
            yield fpath

    def is_supported_extension(self, value):
        if isinstance(value, str):
            return value.lower() in self._supported_extensions
        return False

    def is_valid_file_entry(self, direntry):
        if direntry.is_dir():
            return False
        fname = direntry.name
        if not fname.startswith('.') and fname.endswith(self.ext):
            return True
        return False

    def get_file_entry(self, fname):
        for fentry in self.iter_files():
            if fentry.name == fname:
                return fentry

    def has_file(self, fname):
        for fentry in self.iter_files():
            if fentry.name == fname:
                return True


class PicturesProvider(FileProvider):

    _supported_extensions = ('png', 'jpeg', 'jpg', 'svg', 'ico')


if __name__ == '__main__':
    fp = PicturesProvider('/Users/mero/Desktop', 'ico')
    print(list(fp.iter_files()))
    print(list(fp.build_path_names(*('mero', 'mehdi', 'toto'))))
