# -*- coding: utf-8 -*-
"""The base Controller API."""

from tg import TGController, tmpl_context, RestController
from tg import request

from hexagon.app_settings import COLOR_PALETTE_RGB, generate_hex_palette_from_rgb


__all__ = ['BaseController', 'BaseRestController']


class BaseController(TGController):
    """
    Base class for the controllers in the application.

    Your web application should have one of these. The root of
    your application is used to compute URLs used by your app.

    """

    def __call__(self, environ, context):
        """Invoke the Controller"""
        # TGController.__call__ dispatches to the Controller method
        # the request is routed to.

        tmpl_context.identity = request.identity
        tmpl_context.app_theme_rgb = COLOR_PALETTE_RGB
        tmpl_context.app_theme_hex = generate_hex_palette_from_rgb(COLOR_PALETTE_RGB, css=True)

        return TGController.__call__(self, environ, context)


class BaseRestController(RestController):

    def __call__(self, environ, context):
        """Invoke the Controller"""
        # RestController.__call__ dispatches to the Controller method
        # the request is routed to.

        return RestController.__call__(self, environ, context)
