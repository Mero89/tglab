hexagon package
===============

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   hexagon.analytics
   hexagon.courbe
   hexagon.db
   hexagon.etl
   hexagon.migrate
   hexagon.performance
   hexagon.reporting
   hexagon.repository
   hexagon.test
   hexagon.url_utils
   hexagon.utils
   hexagon.veille

Submodules
----------

hexagon.app\_settings module
----------------------------

.. automodule:: hexagon.app_settings
   :members:
   :undoc-members:
   :show-inheritance:

hexagon.db\_inspect module
--------------------------

.. automodule:: hexagon.db_inspect
   :members:
   :undoc-members:
   :show-inheritance:

hexagon.deposit module
----------------------

.. automodule:: hexagon.deposit
   :members:
   :undoc-members:
   :show-inheritance:

hexagon.devise module
---------------------

.. automodule:: hexagon.devise
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: hexagon
   :members:
   :undoc-members:
   :show-inheritance:
