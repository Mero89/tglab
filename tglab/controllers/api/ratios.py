import datetime
import operator as op

import tg

import tglab.model as db
from tglab.lib.base import BaseRestController

from hexagon.analytics import RatiosEngine
from hexagon.utils import to_date, to_bool


__all__ = ['RatiosService']

RATIOS_SET = {
    'var_historique',
    'volatilite',
    'tracking_error',
    'ratio_sharpe',
    'ratio_information',
    'ratio_success',
    'max_shortfall',
    'tracking_error',
}

class RatiosService(BaseRestController):

    @tg.expose('json')
    def get_all(self, code_fonds: str, date_marche: datetime.date, periode: str='1:A', indice_sr: str='TMP', annualiser: bool=False, actif_moyen: float=1):
        indice_sr = db.Indice.by_code(indice_sr)
        fd = db.Fonds.by_code(code_fonds)
        re = RatiosEngine(fd, indice_sr=indice_sr)
        rv = re.get_all_ratios_periode(date_marche=to_date(date_marche), periode=periode, annualiser=to_bool(annualiser), valeur_moyenne=float(actif_moyen))
        return rv

    @tg.expose('json')
    def get_all_date(self, code_fonds: str, debut: datetime.date, fin: datetime.date, periode: str=None, indice_sr: str='TMP', annualiser: bool=False, actif_moyen: float=1):
        indice_sr = db.Indice.by_code(indice_sr)
        fd = db.Fonds.by_code(code_fonds)
        re = RatiosEngine(fd, indice_sr=indice_sr)
        rv = re.get_all_ratios(debut=to_date(debut), fin=to_date(fin), periode=periode, annualiser=to_bool(annualiser), valeur_moyenne=float(actif_moyen))
        return rv

    @tg.expose('json')
    def get(self, code_fonds: str, code_ratio: str, debut: datetime.date, fin: datetime.date, indice_sr: str='TMP'):
        debut = to_date(debut)
        fin = to_date(fin)
        indice_sr = db.Indice.by_code(indice_sr)
        fd = db.Fonds.by_code(code_fonds)
        re = RatiosEngine(fd, indice_sr=indice_sr)
        ratio_caller = op.methodcaller(code_ratio, debut, fin)
        try:
            rv = ratio_caller(re)
            return rv
        except AttributeError as err:
            print(err)
            tg.redirect('/api/ratios/ratios_error?code_ratio={code_ratio}')

    @tg.expose('json')
    def ratios_error(self, code_ratios: str=''):
        msg = f"Le ratio {code_ratios} demandé n'est pas défini au niveau de la bibliothèque des ratios: {RATIOS_SET}"
        return {'error': msg}
