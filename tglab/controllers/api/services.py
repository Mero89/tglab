from tglab.lib.base import BaseController
from tglab.controllers.api.referentiel import ReferentielServiceController
from tglab.controllers.api.performance import PerformanceServiceController
from tglab.controllers.api.reporting import ReportingService
from tglab.controllers.api.ratios import RatiosService
from tglab.controllers.api.dates_performance import DatesPerformanceService

__all__ = ['APIServicesController']


class APIServicesController(BaseController):
    
    referentiel = ReferentielServiceController()
    performance = PerformanceServiceController()
    ratios = RatiosService()
    reporting = ReportingService()
    dates_performance = DatesPerformanceService()
