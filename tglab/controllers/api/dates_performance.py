import datetime

import tg

from tglab.lib.base import BaseRestController


from hexagon.performance import (
    DateCompteur, market_daily_dates
)

from hexagon.utils import to_date


__all__ = ['DatesPerformanceService']


class DatesPerformanceService(BaseRestController):
    
    @tg.expose('json')
    def default_periodes(self, date_marche: datetime.date):
        dc = DateCompteur(market_daily_dates(code_indice='MASI'))
        per = dc.get_all_periods(to_date(date_marche))
        rv = {'data': [x.to_dict() for x in per]}
        return rv

    @tg.expose('json')
    def date_reference(self, date_marche: datetime.date, periode: str):
        dc = DateCompteur(market_daily_dates(code_indice='MASI'))
        per = dc.date_reference(to_date(date_marche), periode=periode)
        rv = per.to_dict()
        return rv

    @tg.expose('json')
    def between(self, debut, fin, freq='H'):
        deb, fin = to_date(debut), to_date(fin)
        dc = DateCompteur(market_daily_dates(code_indice='MASI'))
        per = dc.between(deb, fin, byfreq=freq)
        rv = {
            'debut': debut,
            'fin': fin,
            'freq': freq,
            'ndates': len(per),
            'data': [x.to_dict() for x in per]
        }
        return rv
