"""Module de centralisation des services liés au calcul de la performance des Fonds, des indices et des benchmarks
Le service sera figé en premier lieu.

+ Next step: créer un écran dynamique de génération et de consultation de la performance avec plus de flexibilité
i.e: Calculer la perf d'un Benchmark AD-HOC avec une composition infinie des indices
  à condition de remplir la condition "Sum(poids_indices) == 1.0"
"""
import datetime

import tg

import tglab.model as db
from tglab.lib.base import BaseRestController


from hexagon.performance import (
    PerformanceComparator,
    FondsPerformance,
    IndicePerformance,
    DEFAULT_METHODE_CALCUL,
)

from hexagon.utils import to_date


__all__ = [
    'PerformanceServiceController',
    'PerformanceIndiceService',
    'PerformanceFondsService',
    'PerformanceBenchmarkService',
]


class PerformanceIndiceService(BaseRestController):
    
    @tg.expose('json')
    def performance(self, code: str, date_marche: datetime.date, date_reference: datetime.date, periode=None, methode_calcul=DEFAULT_METHODE_CALCUL):
        ind = db.Indice.by_code(code.upper())
        ip = IndicePerformance(ind).performance(to_date(date_marche), to_date(date_reference), periode=periode, methode_calcul=methode_calcul)
        return ip.to_dict()

    @tg.expose('json')
    def performance_periode(self, code: str, date_marche: datetime.date, periode: str, methode_calcul: str=DEFAULT_METHODE_CALCUL):
        ind = db.Indice.by_code(code.upper())
        ip = IndicePerformance(ind).performance_periode(to_date(date_marche), periode=periode, methode_calcul=methode_calcul)
        return ip.to_dict()

    @tg.expose('json')
    def performance_range(self, code: str, debut: datetime.date, fin: datetime.date, freq='H', periode=None, methode_calcul: str=DEFAULT_METHODE_CALCUL):
        ind = db.Indice.by_code(code.upper())
        ip = IndicePerformance(ind).performance_by_freq(to_date(debut), fin=to_date(fin), periode=periode, freq=freq, methode_calcul=methode_calcul)
        rv = ip.to_dict()
        return rv

    @tg.expose('json')
    def performance_range_periode(self, code: str, date_marche: datetime.date, periode: str, freq='H', methode_calcul: str=DEFAULT_METHODE_CALCUL):
        ind = db.Indice.by_code(code.upper())
        ip = IndicePerformance(ind).performance_periode_range(to_date(date_marche), periode=periode, freq=freq, methode_calcul=methode_calcul)
        rv = ip.to_dict()
        return rv

    @tg.expose('json')
    def performance_range_indice(self, code: str, debut: datetime.date, fin: datetime.date, freq='H', periode=None, base: int=100, methode_calcul: str=DEFAULT_METHODE_CALCUL):
        ind = db.Indice.by_code(code.upper())
        ip = IndicePerformance(ind).performance_by_freq(to_date(debut), fin=to_date(fin), periode=periode, freq=freq, methode_calcul=methode_calcul)
        ip_indice = ip.as_indice(base=base)
        rv = ip_indice.to_dict()
        return rv

    @tg.expose('json')
    def performance_range_periode_indice(self, code: str, date_marche: datetime.date, periode: str, freq='H', base: int=100, methode_calcul: str=DEFAULT_METHODE_CALCUL):
        ind = db.Indice.by_code(code.upper())
        ip = IndicePerformance(ind).performance_periode_range(to_date(date_marche), periode=periode, freq=freq, methode_calcul=methode_calcul)
        ip_indice = ip.as_indice(base=base)
        rv = ip_indice.to_dict()
        return rv

    @tg.expose('json')
    def performance_default_periodes(self, code: str, date_marche: datetime.date) -> list:
        ind = db.Indice.by_code(code.upper())
        ip = IndicePerformance(ind).performance_default_periodes(to_date(date_marche), periods=None)
        rv = {'data': [x.to_dict() for x in ip]}
        return rv


class PerformanceFondsService(BaseRestController):
    
    @tg.expose('json')
    def performance(self, code: str, date_marche: datetime.date, date_reference: datetime.date, periode=None, methode_calcul=DEFAULT_METHODE_CALCUL):
        fd = db.Fonds.by_code(code.upper())
        fp = FondsPerformance(fd).performance(to_date(date_marche), to_date(date_reference), periode=periode, methode_calcul=methode_calcul)
        return fp.to_dict()

    @tg.expose('json')
    def performance_periode(self, code: str, date_marche: datetime.date, periode: str, methode_calcul: str=DEFAULT_METHODE_CALCUL):
        fd = db.Fonds.by_code(code.upper())
        fp = FondsPerformance(fd).performance_periode(to_date(date_marche), periode=periode, methode_calcul=methode_calcul)
        return fp.to_dict()

    @tg.expose('json')
    def performance_range(self, code: str, debut: datetime.date, fin: datetime.date, freq='H', periode=None, methode_calcul: str=DEFAULT_METHODE_CALCUL):
        fd = db.Fonds.by_code(code.upper())
        fp = FondsPerformance(fd).performance_by_freq(to_date(debut), fin=to_date(fin), periode=periode, freq=freq, methode_calcul=methode_calcul)
        rv = fp.to_dict()
        return rv

    @tg.expose('json')
    def performance_range_periode(self, code: str, date_marche: datetime.date, periode: str, freq='H', methode_calcul: str=DEFAULT_METHODE_CALCUL):
        fd = db.Fonds.by_code(code.upper())
        fp = FondsPerformance(fd).performance_periode_range(to_date(date_marche), periode=periode, freq=freq, methode_calcul=methode_calcul)
        rv = fp.to_dict()
        return rv

    @tg.expose('json')
    def performance_range_indice(self, code: str, debut: datetime.date, fin: datetime.date, freq='H', periode=None, base: int=100, methode_calcul: str=DEFAULT_METHODE_CALCUL):
        fd = db.Fonds.by_code(code.upper())
        fp = FondsPerformance(fd).performance_by_freq(to_date(debut), fin=to_date(fin), periode=periode, freq=freq, methode_calcul=methode_calcul)
        fp_indice = fp.as_indice(base=base)
        rv = fp_indice.to_dict()
        return rv

    @tg.expose('json')
    def performance_range_periode_indice(self, code: str, date_marche: datetime.date, periode: str, freq='H', base: int=100, methode_calcul: str=DEFAULT_METHODE_CALCUL):
        fd = db.Fonds.by_code(code.upper())
        fp = FondsPerformance(fd).performance_periode_range(to_date(date_marche), periode=periode, freq=freq, methode_calcul=methode_calcul)
        fp_indice = fp.as_indice(base=base)
        rv = fp_indice.to_dict()
        return rv

    @tg.expose('json')
    def performance_default_periodes(self, code: str, date_marche: datetime.date) -> list:
        fd = db.Fonds.by_code(code.upper())
        fp = FondsPerformance(fd).performance_default_periodes(to_date(date_marche), periods=None)
        rv = {'data': [x.to_dict() for x in fp]}
        return rv


class PerformanceBenchmarkService(BaseRestController):
    
    @tg.expose('json')
    def performance(self, code: str, date_marche: datetime.date, date_reference: datetime.date, periode=None, methode_calcul=DEFAULT_METHODE_CALCUL):
        fd = db.Fonds.by_code(code.upper())
        fp = PerformanceComparator(fd).performance(to_date(date_marche), to_date(date_reference), periode=periode, methode_calcul=methode_calcul)
        return fp.to_dict()

    @tg.expose('json')
    def performance_periode(self, code: str, date_marche: datetime.date, periode: str, methode_calcul: str=DEFAULT_METHODE_CALCUL):
        fd = db.Fonds.by_code(code.upper())
        fp = PerformanceComparator(fd).performance_periode(to_date(date_marche), periode=periode, methode_calcul=methode_calcul)
        return fp.to_dict()

    @tg.expose('json')
    def performance_range(self, code: str, debut: datetime.date, fin: datetime.date, freq='H', periode=None, methode_calcul: str=DEFAULT_METHODE_CALCUL):
        fd = db.Fonds.by_code(code.upper())
        fp = PerformanceComparator(fd).performance_by_freq(to_date(debut), fin=to_date(fin), periode=periode, freq=freq, methode_calcul=methode_calcul)
        rv = fp.to_dict()
        return rv

    @tg.expose('json')
    def performance_range_periode(self, code: str, date_marche: datetime.date, periode: str, freq='H', methode_calcul: str=DEFAULT_METHODE_CALCUL):
        fd = db.Fonds.by_code(code.upper())
        fp = PerformanceComparator(fd).performance_periode_range(to_date(date_marche), periode=periode, freq=freq, methode_calcul=methode_calcul)
        rv = fp.to_dict()
        return rv

    @tg.expose('json')
    def performance_range_indice(self, code: str, debut: datetime.date, fin: datetime.date, freq='H', periode=None, base: int=100, methode_calcul: str=DEFAULT_METHODE_CALCUL):
        fd = db.Fonds.by_code(code.upper())
        fp = PerformanceComparator(fd).performance_point_by_freq(to_date(debut), fin=to_date(fin), periode=periode, freq=freq, methode_calcul=methode_calcul)
        rv = fp.to_dict()
        return rv

    @tg.expose('json')
    def performance_range_periode_indice(self, code: str, date_marche: datetime.date, periode: str, freq='H', base: int=100, methode_calcul: str=DEFAULT_METHODE_CALCUL):
        fd = db.Fonds.by_code(code.upper())
        fp = PerformanceComparator(fd).performance_point_periode(to_date(date_marche), periode=periode, freq=freq, base=base, methode_calcul=methode_calcul)
        rv = fp.to_dict()
        return rv
    
    @tg.expose('json')
    def performance_default_periodes(self, code: str, date_marche: datetime.date) -> list:
        fd = db.Fonds.by_code(code.upper())
        fp = PerformanceComparator(fd).performance_default_periodes(to_date(date_marche), periods=None)
        rv = {'data': [x.to_dict() for x in fp]}
        return rv

    @tg.expose('json')
    def performance_point_by_freq(self, code: str, debut, fin, freq='H', periode=None, base=100, methode_calcul=DEFAULT_METHODE_CALCUL):
        fd = db.Fonds.by_code(code.upper())
        fp = PerformanceComparator(fd).performance_point_by_freq(to_date(debut), to_date(debut), base=base, periode=periode, freq=freq, methode_calcul=methode_calcul)
        rv = fp.to_dict()
        return rv

    @tg.expose('json')
    def performance_point_periode(self, code: str, date_marche, periode: str, freq='H', base=100, methode_calcul=DEFAULT_METHODE_CALCUL):
        fd = db.Fonds.by_code(code.upper())
        fp = PerformanceComparator(fd).performance_point_periode(to_date(date_marche), periode=periode, freq=freq, base=base, methode_calcul=methode_calcul)
        rv = fp.to_dict()
        return rv


class PerformanceServiceController(BaseRestController):

    indices = PerformanceIndiceService()
    fonds = PerformanceFondsService()
    benchmark = PerformanceBenchmarkService()
