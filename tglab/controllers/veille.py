# -*- coding: utf-8 -*-
"""Veille controller module"""
import datetime

import tg
# from tg import predicates

import tglab.model as db
from tglab.lib.base import BaseController

from hexagon.veille import write_veille_analytics
from hexagon.performance import DateCompteur, market_daily_dates


class VeilleController(BaseController):
    # Uncomment this line if your controller requires an authenticated user
    # allow_only = predicates.not_anonymous()
    page_title = "Formulaire - Marché OPCVM"
    frm_title = "Veille - Marché OPCVM"
    ctxt_title = "Informations contextuelles "
    help_title = "Aide sur le rapport "

    @tg.expose('tglab.templates.forms.veille')
    def index(self, **kw):
        frm_ctxt = self.get_frm_ctxt()
        frm_help = self.get_frm_help()

        frm_dico = {
            'frm_title': self.frm_title,
        }
        # Date lors du chargement de la page
        default_date = datetime.date.today()
        # En attendant de généraliser le concept par un controlleur dédié central
        # qui centralisera l'ensemble des données contextuelles des formulaires
        # This will allow to retrieve data dynamically using a JS framework (JQuery or JQueryUI)
        if frm_ctxt['maxdate']:
            default_date = frm_ctxt['maxdate']

        frm_dico['dt_debut'] = default_date
        frm_dico['dt_fin'] = default_date
        frm_dico['audit'] = False
        frm_dico['data_only'] = True
        req_dico = tg.request.GET
        req_dico['method'] = tg.request.method

        if tg.request.method == 'POST':
            rq = tg.request.POST
            frm_dico['dt_debut'] = datetime.date.fromisoformat(rq['dt_debut'])
            frm_dico['dt_fin'] = datetime.date.fromisoformat(rq['dt_fin'])
            if 'audit' in rq:
                frm_dico['audit'] = True
            else:
                frm_dico['audit'] = False
            if 'data_only' in rq:
                frm_dico['data_only'] = True
            else:
                frm_dico['data_only'] = False
            self.generate_veille_analytics(**frm_dico)
            req_dico = rq
            req_dico['method'] = tg.request.method

        rv = {
            'page': 'veille_opcvm',
            'page_title': self.page_title,
            'frm_dico': frm_dico,
            'frm_help': frm_help,
            'frm_ctxt': frm_ctxt,
            'req_dico': req_dico,
        }
        return rv

    @tg.expose(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    def generate_veille_analytics(self, **kw):
        meth = tg.request.method
        if meth != 'POST':
            return tg.abort(404, f"Methode {meth} non authorisée")
        
        rq = tg.request.POST
        debut = datetime.date.fromisoformat(rq['dt_debut'])
        fin = datetime.date.fromisoformat(rq['dt_fin'])
        audit = False
        if 'audit' in rq:
            audit = True
        
        resp = tg.response
        fname = 'veille_report.xlsx'
        resp.headers['Content-Disposition'] = f'attachment;filename={fname}'
        obuff = write_veille_analytics('', debut=debut, fin=fin, audit=audit, in_memory=True)
        obuff.seek(0)
        return obuff

    def get_frm_ctxt(self) -> dict:
        """Construit un dict contennant les infos contextuelles pour le controlleur"""
        maxdate = db.DBSession.query(db.func.max(db.EtatFondsMarket.date_marche)).first()
        mindate = db.DBSession.query(db.func.min(db.EtatFondsMarket.date_marche)).first()
        if mindate:
            mindate = mindate[0]
        else:
            mindate = None

        if maxdate:
            maxdate = maxdate[0]
            count = db.DBSession.query(
                db.EtatFondsMarket.id
            ).filter(
                db.EtatFondsMarket.date_marche == maxdate
            ).count()
            dc = DateCompteur(market_daily_dates())
            date_ytd = dc.ytd(maxdate).date_reference
        
        else:
            maxdate = count = date_ytd = None

        frm_ctxt = {
            'ctxt_title': self.ctxt_title,
            'maxdate': maxdate,
            'mindate': mindate,
            'curr_ytd': date_ytd,
            'count_fonds': count
        }
        return frm_ctxt

    def get_frm_help(self) -> dict:
        """Construit un dict contennant les infos contextuelles pour le controlleur"""

        # Better fetch it from a file or a folder of HTML files where each file is the help Documentation
        help_text = FRM_HELP

        frm_info = {
            'help_title': self.help_title,
            'frm_help': help_text,
        }

        return frm_info


FRM_HELP = """\
<p><strong>Utilisation:</strong></p>
<div style="padding-left: 30px;">
    <li>Le formulaire permet de g&eacute;n&eacute;rer 2 fichiers Excel (un Template pour la mise en forme du Reporting et un fichier "data")</li>
    <li>Il est possible de ne g&eacute;n&eacute;rer que le fichier de donn&eacute;es <b>"data"</b> en cochant la case <b>"Data Only"</b></li>
    <li>La case <b>"Audit"</b> permet de g&eacute;n&eacute;rer dans le fichier <b>"data"</b> des feuilles suppl&eacute;mentaires contenant les donn&eacute;es et les informations ayant contribu&eacute; &agrave; la g&eacute;n&eacute;ration du rapport.</li>
    <li>Pour g&eacute;n&eacute;rer un reporting au format PDF, ouvrir les deux fichiers t&eacute;l&eacute;charg&eacute;s afin de mettre &agrave; jour les donn&eacute;es du rendu du reporting et enregistrer sous format PDF en utilisant l'outil d'export d'EXCEL.</li>
</div>
"""
