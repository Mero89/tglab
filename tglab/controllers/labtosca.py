import io
import datetime

import tg
import tw2.forms as twf
import tw2.core as twc
# from tg import predicates

import tglab.model as db
from tglab.lib.base import BaseController
# from tglab.lib.widgets import *

from hexagon.performance import DateCompteur, market_daily_dates
from hexagon.utils import groupby_attr, to_date


__all__ = ['ToscaLab']


def populate_fonds(as_grouped=True):
    dico_fonds = groupby_attr(db.Fonds.get_all().all(), 'categorie')
    rv = []
    for (k, v) in dico_fonds.items():
        glist = [(x.id, x.description) for x in v]
        grow = (k, glist)
        rv.append(grow)
    return rv


class ToscaLab(BaseController):
    
    @tg.expose('tglab.templates.labtosca')
    def index(self, **kw):
        frm_title = 'TW2 Form Laboratory (Happy Learning!)'
        twdico = {
            'value': 'Mero',
            # 'wdgt_list': list(range(4)),
        }
        rv = {
            'page': 'lab',
            'page_title': 'Labo des Formulaires TW2',
            'frm_title': frm_title,
            'twform': DateForm,
            'twdico': twdico
            # 'twdico': None
        }
        return rv

    @tg.expose('json')
    def show_submit(self, *args, **kw):
        print(tg.request.method)
        rq = tg.request.POST
        print(rq)
        rv = {}
        if tg.request.method == 'POST':
            rv = {
                'debut': rq['debut'],
                'fin': rq['fin'],
                'fonds': rq['fonds'],
                'adate': rq['adate'],
            }
        return rv


class BSDateWidget(twc.Widget):
    inline_engine_name = "jinja"
    template = """
    <div class="row">
      {# <label class="control-label">{{w.label}}</label> #}
      <input class="form-control" type="date" value="{{ w.value }}" name="{{ w.name }}" min="{{ w.mindate }}" max="{{ w.maxdate }}" placeholder="{{ w.placeholder }}" autofocus/>
    </div>
    """


class DateForm(twf.Form):
    class child(twf.TableLayout):
        debut = twf.CalendarDatePicker(
            # value=to_date('22/07/2020'),
            calendar_lang='fr',
            date_format='%d/%m/%Y',
            button_text='Choisir',
            validator=twc.validation.DateValidator(max=to_date('29/05/2020'))
        )
        fin = twf.CalendarDatePicker(
            # value=to_date('22/07/2020'),
            calendar_lang='fr',
            date_format='%d/%m/%Y',
            button_text='Choisir',
            validator=twc.validation.DateValidator(max=to_date('29/05/2020'))
        )
        # adate = BSDateWidget(label='adate', name='adate', value=to_date('22/07/2019'), mindate=to_date('22/07/2018'), maxdate=to_date('22/07/2020'))
        fonds = twf.MultipleSelectField(options=populate_fonds())
        fichier = twf.FileField(filename='fichier')

    action = '/lab/show_submit'
    buttons = [
        twf.SubmitButton(value="Submit"),
        twf.ResetButton(),
        # twf.Button(value="Say Hi", attrs=dict(onclick="alert('hi')"))
    ]
