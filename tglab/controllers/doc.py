# -*- coding: utf-8 -*-
"""Documentation of Hexagon Core"""
import pathlib
import operator as op
import tg
from hexagon.etl import ExcelIn
# from tg import predicates

from tglab.lib.base import BaseController
from tglab.lib import PUBLIC_PATH


DATASTORE_PATH = PUBLIC_PATH.joinpath('datastore')


class DocController(BaseController):
    # Uncomment this line if your controller requires an authenticated user
    # allow_only = predicates.not_anonymous()
    page_title = "Documentation - Hexagon Core"

    @tg.expose('tglab.templates.documentation')
    def index(self, **kw):
        return dict(page='doc')

    @tg.expose('tglab.templates.forms.reporting_engine_keywords')
    def keywords(self, **kw):
        # here = pathlib.Path(__file__).absolute().parent
        # fpath = here.parent.joinpath('public/src/keywords.xlsx')
        fpath = DATASTORE_PATH.joinpath('keywords.xlsx')
        print(fpath)
        ei = ExcelIn(str(fpath))
        keywords_list = ei.read_dict(0)
        keywords_list.sort(key=op.itemgetter('keys'))
        ei.close()
        rv = {
            'page': 'keywords',
            'keywords_list': keywords_list
        }
        return rv
