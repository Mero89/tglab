import datetime

import tg
# from tg import predicates

import tglab.model as db
from tglab.lib.base import BaseController

from hexagon.db_inspect import dates_indices, inspect_raw_tables


class DBInspectController(BaseController):

    page_title = "Inspect - DataBase"

    @tg.expose('tglab.templates.inspect_indices')
    def inspect_indices(self, **kw):
        data = {
            'indice_value': dates_indices(),
            'raw_indice_value': inspect_raw_tables()
        }
        rv = {'data': data}
        return rv
