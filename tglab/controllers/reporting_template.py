import datetime

import tg
# from tg import predicates

import tglab.model as db
from tglab.lib.base import BaseController

from hexagon.performance import DateCompteur, market_daily_dates


class ReportingOpcvmTemplateController(BaseController):
    # Uncomment this line if your controller requires an authenticated user
    # allow_only = predicates.not_anonymous()
    page_title = "Template - Generator"
    frm_title = "Template Generator - OPCVM"

    @tg.expose('tglab.templates.forms.reporting_template_opcvm')
    def index(self, **kw):
        frm_ctxt = self.get_frm_ctxt()
        categories = ['MON', 'OCT', 'OMLT', 'ACT', 'DIV']
        frm_dico = {
            'frm_title': self.frm_title,
            'liste_categories': categories
        }
        # Date lors du chargement de la page
        default_date = datetime.date.today()
        # En attendant de généraliser le concept par un controlleur dédié central
        # qui centralisera l'ensemble des données contextuelles des formulaires
        # This will allow to retrieve data dynamically using a JS framework (JQuery or JQueryUI)
        if frm_ctxt['maxdate']:
            default_date = frm_ctxt['maxdate']

        frm_dico['date_debut'] = default_date
        frm_dico['date_fin'] = default_date
        code_options = ['MENSUEL', 'HEBDO']
        frm_dico['liste_periodes'] = code_options
        # Inspect dico

        rv = {
            'page': 'reporting_opcvm',
            'page_title': self.page_title,
            'frm_dico': frm_dico,
            'frm_ctxt': frm_ctxt
        }
        return rv

    @tg.expose('tglab.templates.forms.auto_fill_one')
    def auto_fill_one(self, **kw):
        frm_ctxt = self.get_frm_ctxt()
        frm_dico = {
            'frm_title': self.frm_title        }
        # Date lors du chargement de la page
        default_date = datetime.date.today()
        # En attendant de généraliser le concept par un controlleur dédié central
        # qui centralisera l'ensemble des données contextuelles des formulaires
        # This will allow to retrieve data dynamically using a JS framework (JQuery or JQueryUI)
        if frm_ctxt['maxdate']:
            default_date = frm_ctxt['maxdate']

        frm_dico['date_debut'] = default_date
        frm_dico['date_fin'] = default_date
        code_options = ['MENSUEL', 'HEBDO']
        frm_dico['liste_periodes'] = code_options
        code_options = list(sorted(x.code for x in db.Fonds.get_all()))
        frm_dico['liste_fonds'] = code_options
        # Inspect dico

        rv = {
            'page': 'reporting_opcvm',
            'page_title': self.page_title,
            'frm_dico': frm_dico,
            'frm_ctxt': frm_ctxt
        }
        return rv

    def get_frm_ctxt(self) -> dict:
        """Construit un dict contennant les infos contextuelles pour le controlleur"""
        maxdate = db.DBSession.query(db.func.max(db.EtatFonds.date_marche)).first()

        if maxdate:
            maxdate = maxdate[0]
        else:
            maxdate = None

        frm_ctxt = {
            'maxdate': maxdate,
        }
        return frm_ctxt
