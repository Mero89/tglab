"""Module de génération des données de reporting selon la méthode détaillée sur le document de BackUp
en éliminatn les étapes du notebook, Now everything is in a Web UI.
"""
# -*- coding: utf-8 -*-
"""Veille controller module"""
import datetime

import tg
# from tg import predicates

import tglab.model as db
from tglab.lib.base import BaseController

from hexagon.reporting import write_reporting
from hexagon.performance import DateCompteur, market_daily_dates


class ReportingOpcvmController(BaseController):
    # Uncomment this line if your controller requires an authenticated user
    # allow_only = predicates.not_anonymous()
    page_title = "Formulaire - Reporting OPCVM"
    form_title = "Données de Reporting - OPCVM"

    @tg.expose('tglab.templates.forms.reporting_opcvm')
    def index(self, **kw):
        form_assist = self.build_assist_data()

        form_data = {
            'form_title': self.form_title,
        }
        # Date lors du chargement de la page
        default_date = datetime.date.today()
        # En attendant de généraliser le concept par un controlleur dédié central
        # qui centralisera l'ensemble des données contextuelles des formulaires
        # This will allow to retrieve data dynamically using a JS framework (JQuery or JQueryUI)
        if form_assist['maxdate']:
            default_date = form_assist['maxdate']

        form_data['date_debut'] = default_date
        form_data['date_fin'] = default_date
        code_options = list(sorted(x.code for x in db.Fonds.get_all()))
        form_data['liste_fonds'] = code_options

        rv = {
            'page': 'reporting_opcvm',
            'page_title': self.page_title,
            'form_data': form_data,
            'form_assist': form_assist,
        }
        return rv

    @tg.expose(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    def generate_reporting_analytics(self, **kw):
        # tg.flash('Called Reporting')
        meth = tg.request.method
        if meth != 'POST':
            return tg.abort(404, f"Methode {meth} non authorisée")
        try:
            rq = tg.request.POST
            debut = datetime.date.fromisoformat(rq['date_debut'])
            fin = datetime.date.fromisoformat(rq['date_fin'])
            code_fonds = rq['code_fonds']
            resp = tg.response
            fname = f'{code_fonds.lower()}_data.xlsx'
            resp.headers['Content-Disposition'] = f'attachment;filename={fname}'
            obuff = write_reporting('', code_fonds, date_reporting=fin, date_reference=debut, in_memory=True)
            obuff.seek(0)
            return obuff
        except KeyError as err:
            msg = f"La clé demandée est introuvable {err.args}"
            return tg.abort(500, msg)

    def build_assist_data(self) -> dict:
        """Construit un dict contennant les infos contextuelles pour le controlleur"""
        maxdate = db.DBSession.query(db.func.max(db.EtatFonds.date_marche)).first()
        if maxdate:
            maxdate = maxdate[0]
        
        form_assist = {
            'maxdate': maxdate,
        }
        return form_assist
