# -*- coding: utf-8 -*-
"""Main Controller"""
from tg import (
    predicates, expose, flash, url, lurl,
    request, redirect, tmpl_context, response
)
# from tg.exceptions import HTTPFound
# from tg.i18n import ugettext as _, lazy_ugettext as l_

# from tgext.admin.controller import AdminController
# from tgext.admin.tgadminconfig import BootstrapTGAdminConfig as TGAdminConfig

from tglab.lib.base import BaseController
# from tglab.controllers.secure import SecureController
from tglab.controllers.error import ErrorController
from tglab.controllers.veille import VeilleController
from tglab.controllers.doc import DocController
from tglab.controllers.reporting_opcvm import ReportingOpcvmController
from tglab.controllers.reporting_template import ReportingOpcvmTemplateController
from tglab.controllers.api import APIServicesController
from tglab.controllers.inspect_db import DBInspectController
from tglab.controllers.labtosca import ToscaLab


class RootController(BaseController):
    """
    The root controller for the tglab application.

    All the other controllers and WSGI applications should be mounted on this
    controller. For example::

        panel = ControlPanelController()
        another_app = AnotherWSGIApplication()

    Keep in mind that WSGI applications shouldn't be mounted directly: They
    must be wrapped around with :class:`tg.controllers.WSGIAppController`.

    """
    veille = VeilleController()
    api = APIServicesController()
    reporting_opcvm = ReportingOpcvmController()
    template_generator = ReportingOpcvmTemplateController()
    doc = DocController()
    inspect = DBInspectController()
    lab = ToscaLab()
    error = ErrorController()
    
    def _before(self, *args, **kw):
        tmpl_context.project_name = "Hexagon"
        response.headers.update({'Access-Control-Allow-Origin': '*'})

    @expose('tglab.templates.index')
    def index(self):
        """Handle the front-page."""
        return dict(page='index')


__all__ = ['RootController']
