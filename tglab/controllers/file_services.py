"""Module de services de fichiers:

    ** Transform MBI
    ** Transform ASFIM
    ** Transform MaroClear
    ** Download Cours de Devises
    ** Download Courbe des taux
    ** Download ASFIM (WEB Scrapping)
"""
import io
import datetime
import tempfile
import operator as op
from collections import namedtuple

import tg
import xlsxwriter
# from tg import predicates

# import tglab.model as db
from tglab.lib.base import BaseRestController

from hexagon.etl import ExcelOut
from hexagon.etl.transform.transform_mbi import read_mbi_file

__all__ = ['FileServices']


class FileServices(BaseRestController):

    @tg.expose(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    def transform_mbi(self, file_input, start_col=1, start_row=12, end_col=26):
        sheet_name = 'raw_mbi'
        f = tempfile.NamedTemporaryFile()
        gen = read_mbi_file(f, start_col=start_col, start_row=start_row, end_col=end_col)
        
        mbi_row = namedtuple('mbi_row', ['date_marche', 'code_strate', 'value', 'duration', 'sensibilite', 'ytm', 'coupon'])
        # Consume the generator and sort the data on the "strate" Key Value.
        ldata = sorted((mbi_row(*el) for el in gen), key=op.attrgetter('date_marche'))
        
        outbuff = io.BytesIO()
        wb = xlsxwriter.Workbook(outbuff)
        eo = ExcelOut(constant_memory=True, wbook=wb)
        eo.write_collection(ldata, sheet_name, 0, 0, attr_list=mbi_row._fields)
        eo.close()
        outbuff.seek(0)
        return outbuff

    @tg.expose(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    def transform_asfim(self, file_input):
        pass

    @tg.expose(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    def transform_maroclear(self, file_input):
        pass
