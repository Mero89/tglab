# -*- coding: utf-8 -*-

#  Quickstarted Options:
#
#  sqlalchemy: True
#  auth:       sqlalchemy
#  mako:       False
#
#

# This is just a work-around for a Python2.7 issue causing
# interpreter crash at exit when trying to log an info message.
try:
    import logging
    import multiprocessing
except:
    pass

import sys
py_version = sys.version_info[:2]

try:
    from setuptools import setup, find_packages

except ImportError:
    from ez_setup import use_setuptools
    use_setuptools()
    from setuptools import setup, find_packages

testpkgs = [
    'WebTest >= 1.2.3',
    'nose',
    'coverage',
    'gearbox'
]

install_requires = [
    "TurboGears2 >= 2.4.3",
    "tg.devtools",
    "tgext.crud",
    "tgext.admin >= 0.6.1",
    "cherrypy",
    "Beaker >= 1.8.0",
    "tw2.forms",
    "WebHelpers2",
    "sqlalchemy",
    "zope.sqlalchemy >= 1.2",
    "alembic",
    "jinja2",
    "Kajiki >= 0.6.3",
    "Babel",
    "repoze.who",
    "xlsxwriter",
    "requests",
    "numpy",
    "pandas",
]

setup(
    name='tglab',
    version='1',
    description="DataStore en mode intranet avec une petite couche d'authenfication.",
    author='FAKIR Marouane @CKG',
    author_email='mfakir@cdgcapitalgestion.ma',
    url='cdgcapitalgestion.ma',
    packages=find_packages(exclude=['ez_setup']),
    install_requires=install_requires,
    include_package_data=True,
    test_suite='nose.collector',
    tests_require=testpkgs,
    extras_require={
        'testing': testpkgs
    },
    package_data={'tglab': [
        'i18n/*/LC_MESSAGES/*.mo',
        'templates/*/*',
        'public/*/*'
    ]},
    message_extractors={'tglab': [
        ('**.py', 'python', None),
        ('templates/**.xhtml', 'kajiki', {'strip_text': False, 'extract_python': True}),
        ('templates/**.jinja', 'jinja2', None),
        ('public/**', 'ignore', None)
    ]},
    entry_points={
        'paste.app_factory': [
            'main = tglab.config.application:make_app'
        ],
        'gearbox.plugins': [
            'turbogears-devtools = tg.devtools'
        ]
    },
    zip_safe=False
)
